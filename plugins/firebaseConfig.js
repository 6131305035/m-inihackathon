import firebase from 'firebase/app'
import 'firebase/firestore'

// Initialize Cloud Firestore through Firebase
if (!firebase.apps.length) {
    const firebaseConfig = {
          apiKey: "AIzaSyDDFzk0pzDlYT4X_LfVjgQFu43iAZDWEsw",
          authDomain: "nuxtjslearn.firebaseapp.com",
          databaseURL: "https://nuxtjslearn.firebaseio.com",
          projectId: "nuxtjslearn",
          storageBucket: "nuxtjslearn.appspot.com",
          messagingSenderId: "394343772116",
          appId: "1:394343772116:web:51edddd6d93baac5ba7041",
          measurementId: "G-7WMG2J9ECV"
    }
    firebase.initializeApp(firebaseConfig)
}

export const db = firebase.firestore()