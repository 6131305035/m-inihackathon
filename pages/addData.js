
import { db } from '../plugins/firebaseConfig';
import firebase from 'firebase/app'

addData = () => {
    // เก็บข้อมูล Form ใน collection MyForm ( มี 1 document แต่จะ update ข้อมูลเรื่อย ๆ )
    var data = {
        txt: this.txt,
        checkbox: this.checkbox,
        radioGroup: this.radioGroup,
        switcher: this.switcher,
        rating: this.rating,
        slider: this.slider,
    };
    db.collection("MyForm").doc("formdata").set(data)
        .then(function () {
            console.log("Document successfully written! -> MyForm");
        })
        .catch(function (error) {
            console.error("Error writing document: ", error);
        });

    // เก็บข้อมูล Input Text ใน collection MyText (มีหลาย document ข้อมูลจะเพิ่มขึ้นเรื่อย ๆ )
    var dataText = {
        txt: this.txt,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
    }
    db.collection("MyText").doc().set(dataText)
        .then(function () {
            console.log("Document successfully written! -> MyText");
        })
        .catch(function (error) {
            console.error("Error writing document: ", error);
        });
}