

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const con = require('./connect_yakkawdb');
const prefix = "/api/v1";

//import * as utils from "./utils/utils";




const app = express();
const onPort = 9001;

app.use(cors({ origin : true }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));

    //Get All

    app.get( prefix + "/devinfo",(req , res) => {
        let sql = "select * from `devinfo` "
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            console.log(results)

            return res.send(results);
        });
        //res.send('<h1>HELLO HUSKER GAME 04_06_1999</h1>');
        //res.send('<h1>Welcome To Komgrich Srisak</h1>');
        //res.writeHead(200,{'Content-type':'text/html' });
        //var myStream = fs.createReadStream(__dirname + "/" + 'testPage.html' , 'utf8');
    });

    //Get By ID

    app.get(prefix + "/devinfo/:id",(req , res) => {
        let sql = "select * from `devinfo` where id = " + req.params.id
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            
            console.log(results[0])

            return res.send(results[0]);
        });
        
    });

    //Create User

    app.post(prefix + '/devinfo/', function(req,res) {
        
        //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
        let data = req.body;
        
        if (!data) {
            return res.status(400).send({ success : false, message : 'Please Provide body' })
        }

        con.query("insert into `devinfo` set ?", data , function (error, results , fields) {
            if (error) throw error ;
            console.log("save appt ---->");
            console.log(results)
            return res.send({ success: true, data: results, message: 'New inserting successfully.' });
        });

    });

    //Update User

    app.put(prefix + '/devinfo/:id', function ( req , res ) {
        let data = req.body;
        var id = req.params.id;
        delete data.id;

        if (!data) {
            return res.status(400).send({ error : true , message : 'Please provide body data' });
        }
        
        con.query ("update `devinfo` set ? where `id` = ?", [data,id] 
        , function (error , results , fields) {
            if ( error ) throw error ;
            return res.send({ success : true , data : results , message : 'Update xxx successfully' });
        })
    })

    //Delete User
    app.delete(prefix + '/devinfo/:id', function ( req , res ) {
        let id = req.params.id;
        if(!id) {
            return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
        }
        con.query('delete from `devinfo` where id = ?', [id], function (error , results , fields ) {
            if (error) throw error;
            return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
        });
        console.log(typeof(id))
    });


    //Find By name
    app.get(prefix + '/devinfo_by_name/:name', ( req , res  ) => {
        //let sql = "select * from `customers` where id = " + req.params.id
        let name = req.params.name + '';
        let queName  = `select * from devinfo where deviceid = '${deviceid}'` ;
        let query = con.query(queName, (error,results) => {
            if (error) { console.log(error) }
            
            console.log(results)

            return res.send(results);
        })
        
    })

    
    /*

    section mapdata------------

    */
    //Get All

    app.get(prefix + "/mapdata",(req , res) => {
        let sql = "select * from `mapdata` "
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            console.log(results)

            return res.send(results);
        });
        //res.send('<h1>HELLO HUSKER GAME 04_06_1999</h1>');
        //res.send('<h1>Welcome To Komgrich Srisak</h1>');
        //res.writeHead(200,{'Content-type':'text/html' });
        //var myStream = fs.createReadStream(__dirname + "/" + 'testPage.html' , 'utf8');
    });

    //Get By ID

    app.get(prefix + "/mapdata/:id",(req , res) => {
        let sql = "select * from `mapdata` where id = " + req.params.id
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            
            console.log(results[0])

            return res.send(results[0]);
        });
        
    });

    //Create User

    app.post(prefix + '/mapdata/', function(req,res) {
        
        //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
        let data = req.body;
        
        if (!data) {
            return res.status(400).send({ success : false, message : 'Please Provide body' })
        }

        con.query("insert into `mapdata` set ?", data , function (error, results , fields) {
            if (error) throw error ;
            console.log("save appt ---->");
            console.log(results)
            return res.send({ success: true, data: results, message: 'New inserting successfully.' });
        });

    });

    //Update User

    app.put(prefix + '/mapdata/:id', function ( req , res ) {
        let data = req.body;
        var id = req.params.id;
        delete data.id;

        if (!data) {
            return res.status(400).send({ error : true , message : 'Please provide body data' });
        }
        
        con.query ("update `mapdata` set ? where `id` = ?", [data,id] 
        , function (error , results , fields) {
            if ( error ) throw error ;
            return res.send({ success : true , data : results , message : 'Update xxx successfully' });
        })
    })

    //Delete User
    app.delete(prefix + '/mapdata/:id', function ( req , res ) {
        let id = req.params.id;
        if(!id) {
            return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
        }
        con.query('delete from `mapdata` where id = ?', [id], function (error , results , fields ) {
            if (error) throw error;
            return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
        });
        console.log(typeof(id))
    });


    //Find By name
    app.get(prefix + '/mapdata_by_name/:name', ( req , res  ) => {
        //let sql = "select * from `customers` where id = " + req.params.id
        let name = req.params.name + '';
        let queName  = `select * from mapdata where deviceid = '${deviceid}'` ;
        let query = con.query(queName, (error,results) => {
            if (error) { console.log(error) }
            
            console.log(results)

            return res.send(results);
        })
        
    })

    //----USER___CITY__SECTION --------
    //Get All

    app.get(prefix + "/users",(req , res) => {
        let sql = "select * from `user` "
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            console.log(results)

            return res.send(results);
        });
        //res.send('<h1>HELLO HUSKER GAME 04_06_1999</h1>');
        //res.send('<h1>Welcome To Komgrich Srisak</h1>');
        //res.writeHead(200,{'Content-type':'text/html' });
        //var myStream = fs.createReadStream(__dirname + "/" + 'testPage.html' , 'utf8');
    });

    //Get By ID

    app.get(prefix + "/users/:id",(req , res) => {
        let sql = "select * from `user` where id = " + req.params.id
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            
            console.log(results[0])

            return res.send(results[0]);
        });
        
    });

    //Create User

    app.post(prefix + '/users/', function(req,res) {
        
        //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
        let data = req.body;
        
        if (!data) {
            return res.status(400).send({ success : false, message : 'Please Provide body' })
        }

        con.query("insert into `user` set ?", data , function (error, results , fields) {
            if (error) throw error ;
            console.log("save appt ---->");
            console.log(results)
            return res.send({ success: true, data: results, message: 'New inserting successfully.' });
        });

    });

    //Update User

    app.put(prefix + '/users/:id', function ( req , res ) {
        let data = req.body;
        var id = req.params.id;
        delete data.id;

        if (!data) {
            return res.status(400).send({ error : true , message : 'Please provide body data' });
        }
        
        con.query ("update `user` set ? where `id` = ?", [data,id] 
        , function (error , results , fields) {
            if ( error ) throw error ;
            return res.send({ success : true , data : results , message : 'Update xxx successfully' });
        })
    })

    //Delete User
    app.delete(prefix + '/users/:id', function ( req , res ) {
        let id = req.params.id;
        if(!id) {
            return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
        }
        con.query('delete from `user` where id = ?', [id], function (error , results , fields ) {
            if (error) throw error;
            return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
        });
        console.log(typeof(id))
    });


    //Find By name
    app.get(prefix + '/users_by_name/:name', ( req , res  ) => {
        //let sql = "select * from `customers` where id = " + req.params.id
        let name = req.params.name + '';
        let queName  = `select * from user where id = '${id}'` ;
        let query = con.query(queName, (error,results) => {
            if (error) { console.log(error) }
            
            console.log(results)

            return res.send(results);
        })
        
    })




app.listen(onPort, () => {
console.log("Server is running on port ",onPort)
})

module.exports = app;


