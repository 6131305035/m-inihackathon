const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const con = require('./connect_yakkawdb');


const app = express();
const onPort = 5555;

app.use(cors({ origin : true }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));

    //Get All

    app.get("/users",(req , res) => {
        let sql = "select * from `devinfo` "
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            console.log(results)

            return res.send(results);
        });
        //res.send('<h1>HELLO HUSKER GAME 04_06_1999</h1>');
        //res.send('<h1>Welcome To Komgrich Srisak</h1>');
        //res.writeHead(200,{'Content-type':'text/html' });
        //var myStream = fs.createReadStream(__dirname + "/" + 'testPage.html' , 'utf8');
    });

    //Get By ID

    app.get("/users/:id",(req , res) => {
        let sql = "select * from `devinfo` where id = " + req.params.id
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            
            console.log(results[0])

            return res.send(results[0]);
        });
        
    });

    //Create User

    app.post('/users/', function(req,res) {
        
        //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
        let data = req.body;
        
        if (!data) {
            return res.status(400).send({ success : false, message : 'Please Provide body' })
        }

        con.query("insert into `devinfo` set ?", data , function (error, results , fields) {
            if (error) throw error ;
            console.log("save appt ---->");
            console.log(results)
            return res.send({ success: true, data: results, message: 'New inserting successfully.' });
        });

    });

    //Update User

    app.put('/users/:id', function ( req , res ) {
        let data = req.body;
        var id = req.params.id;
        delete data.id;

        if (!data) {
            return res.status(400).send({ error : true , message : 'Please provide body data' });
        }
        
        con.query ("update `devinfo` set ? where `id` = ?", [data,id] 
        , function (error , results , fields) {
            if ( error ) throw error ;
            return res.send({ success : true , data : results , message : 'Update xxx successfully' });
        })
    })

    //Delete User
    app.delete('/users/:id', function ( req , res ) {
        let id = req.params.id;
        if(!id) {
            return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
        }
        con.query('delete from `devinfo` where id = ?', [id], function (error , results , fields ) {
            if (error) throw error;
            return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
        });
        console.log(typeof(id))
    });


    //Find By name
    app.get('/users_by_name/:name', ( req , res  ) => {
        //let sql = "select * from `customers` where id = " + req.params.id
        let name = req.params.name + '';
        let queName  = `select * from devinfo where deviceid = '${deviceid}'` ;
        let query = con.query(queName, (error,results) => {
            if (error) { console.log(error) }
            
            console.log(results)

            return res.send(results);
        })
        
    })

app.listen(onPort, () => {
console.log("Server is running on port ",onPort)
})

module.exports = app;
