const devPort_devinfo_url = "http://localhost:5555/api/v1/devinfo" ;
const devPort_users_url = "http://localhost:5555/api/v1/users" ;
const devPort_mapdataurl = "http://localhost:5555/api/v1/mapdata";



const ProductPort_devinfo_url = "http://localhost:9001/api/v1/devinfo" ;
const ProductPort_users_url = "http://localhost:9001/api/v1/users" ;
const ProductPort_mapdataurl = "http://localhost:9001/api/v1/mapdata";
module.exports = { 
    devPort_devinfo_url,
    devPort_mapdataurl,
    devPort_users_url,
    ProductPort_devinfo_url,
    ProductPort_mapdataurl,
    ProductPort_users_url } 